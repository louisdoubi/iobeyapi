package com.test.iobeyapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class IobeyapiApplication {

	public static void main(String[] args) {
		SpringApplication.run(IobeyapiApplication.class, args);
	}

}
